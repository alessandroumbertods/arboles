/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.seed.ListaS;

/**
 *
 * @author Estudiantes
 */
public class TestListaS {

    public static void main(String[] args) {

        //Clase parametrizada : Integer
        ListaS<Integer> lista = new ListaS();
        lista.insertarFin(4);
        lista.insertarFin(1);
        lista.insertarFin(5);
        lista.insertarFin(2);
        lista.insertarFin(0);
        

        ListaS<Integer> lista2 = new ListaS();
        lista2.insertarFin(7);
        /*
        lista2.insertarFin(5);
        lista2.insertarFin(6);
        lista2.insertarFin(7);
        lista2.insertarFin(8);
        lista2.insertarFin(9);
         */
 /*
        System.out.println(lista.toString());
        System.out.println("Pos 2:" + lista.get(2));
        lista.set(2, -100);
        System.out.println(lista.toString());
        
        // un ejemplo en extremo CANDIDO :(
        //simulo un vector
        for(int i=0;i<lista.getSize();i++)
            System.out.println("Pos("+i+"):"+lista.get(i));
        
        
        //Concatenando:
        
        lista.addAll(lista2);
        System.out.println("Concatenando:");
        System.out.println(lista);
        System.out.println(lista2);
        System.out.println("Sublista de 3 a 5  " + lista.subList(3,5));
         */
        System.out.println("Lista1: " + lista.toString());
        //System.out.println("Tiene repetidos? " + ((lista.repetidos()) ? "Si" : "No"));
        //System.out.println("Eliminar repetidos: ");
        //lista.eliminarRepetidos();
        //System.out.println(lista);

        System.out.println("Lista2: " + lista2.toString());
        /*
        System.out.println("Tiene repetidos? " + ((lista2.repetidos())? "Si": "No"));
         */
        //System.out.println("Lista 1 y Lista2 son iguales? " + lista.listaRepetida(lista, lista2));
        //System.out.println("Unir lista 1 y 2: " + lista.crearLista(lista, lista2));
        //ListaS<Integer> nueva = lista.ordenarAscendente(lista2);
        //System.out.println("Unir las listas en orden ascendente: " + nueva);
        /*
        lista.sort(lista);
        System.out.println("Ordenar lista " + lista.toString());
         
        //nueva.eliminarRepetidos();
        //System.out.println("Lista eliminando los repetidos: " + nueva);
        for (ListaS x : lista.split(5)) {
            if (x == null) {
                break;
            } else {
                System.out.println(x);
            }
        }
        */
        
        //lista.invertirLista();
        //System.out.println("Lista Invertida: " + lista);
        //lista.removerElementoMasGrande();
        //System.out.println("Lista eliminando el elemento mas grande: " + lista.toString());
       // lista.datoBomba(1); 
        //System.out.println("Dato Boma: " + lista);
        //lista.setNodo(2,9);
        //System.out.println("Lista cambiando en la posicion 2 elemento 9: " + lista);
        //lista.addAll(2, lista2);
        //System.out.println("En la posicion 2, concateno lista 2: " +  lista);
        //System.out.println("Sublista de 1 a 2  " + lista.subList(1,2));
        for (ListaS x : lista.split(5)) {
            if (x == null) {
                break;
            } else {
                System.out.println(x);
            }
        }
    }

}
