/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Cola;
import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author DOCENTE
 */
public class TestValidador_Agrupamiento {

    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * ( { [
         * Ejemplo: L={aa} => --> V L={[aa]} => --> F L=(a)[a]{b}--> V
         */
        //Metiendo los numeros
        /*
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        Pila<Character> p = new Pila();
        for (int i = 0; i < s.length(); i++) {
            char caracter = s.charAt(i);
            if (caracter == ')' || caracter == ']' || caracter == '}') {
                char simboloContrario = 0;
                if (caracter == ')') {
                    simboloContrario = '(';
                }
                if (caracter == ']') {
                    simboloContrario = '[';
                }
                if (caracter == '}') {
                    simboloContrario = '{';
                }

                while (!p.isVacia() && p.top() != simboloContrario) {
                    p.pop();
                }
                if (p.isVacia()) {
                    System.out.println("Mal agrupamiento");
                } else {
                    p.pop();
                }
            } else {
                p.push(caracter);
            }
        }

    if(p.isVacia ()){
            System.out.println("Esta Correctamente Agrupado");
    }
}
         */
        //Sin meter numeros
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        Pila<Character> p = new Pila();
        Cola<Character> orden = new Cola();
        boolean flag = false;
        char arr[] = { '(', '{' , '[' };
        for (int i = 0; i < s.length(); i++) {
            char caracter = s.charAt(i);
            if (caracter == '(' || caracter == '[' || caracter == '{') {
                p.push(caracter);
            } else {
                char simboloContrario = 0;
                if (caracter == ')') {
                    simboloContrario = '(';
                }
                if (caracter == ']') {
                    simboloContrario = '[';
                }
                if (caracter == '}') {
                    simboloContrario = '{';
                }

                if (p.top() == simboloContrario) {
                    orden.enColar(p.pop());
                }
                if (!p.isVacia()) {
                    flag = true;
                }
            }
        }

        if (p.isVacia()) {
            if (flag) {
                System.out.println("Esta Correctamente Agrupado");
            } else {
                int i = 0;
                String ordenado = "";
                while (!orden.isVacia()) {
                    char simbolo = orden.deColar();
                    if (arr[i] == simbolo) {
                        ordenado += simbolo;
                        i++;
                    }
                }
                if (ordenado.equals("({[")) {
                    System.out.println("Esta Correctamente Agrupado");
                }
            }
        }
    }
}
