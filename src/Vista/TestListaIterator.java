/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaS;
import java.util.Random;
import Util.seed.Conjunto;
import java.util.Iterator;

/**
 *
 * @author docente
 */
public class TestListaIterator {

    public static void main(String[] args) {

        //ListaS<Integer> l1 = crear(100000);
        Conjunto<Integer> c2 = crearConjunto(10);
        Conjunto<Integer> c1 = crearConjunto(10);
        System.out.println("1 " + c2.toString());
        System.out.println("2 " + c1.toString());
        //sumaCandida(l1);
        //sumaBacana(l1);
        imprimir(c1.getUnion(c2));
    }

    private static void sumaBacana(ListaS<Integer> l) {
        long s = 0;
        for (Integer x:l) {
            s += x; //--> llama n veces a getPos() y en cada iteración recorre desde la cabeza
        }
        System.out.println("La suma es:" + s);
    }

    private static void sumaCandida(ListaS<Integer> l) {
        long s = 0;
        for (int i = 0; i < l.getSize(); i++) {
            s += l.get(i); //--> llama n veces a getPos() y en cada iteración recorre desde la cabeza
        }
        System.out.println("La suma es:" + s);
    }

    private static ListaS<Integer> crear(int n) {

        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        ListaS<Integer> l = new ListaS();
        while (n > 0) {
            l.insertarInicio(new Random().nextInt(n));
            n--;
        }
        return l;
    }

    private static Conjunto<Integer> crearConjunto(int n) {
        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        Conjunto<Integer> c = new Conjunto();
        int noRepetidos[] = new int[21];
        while(n > 0){
            int random = new Random().nextInt(20);
            while(noRepetidos[random] == random){
                 random = new Random().nextInt(20);
            }
            c.insertar(random);
            noRepetidos[random] = random;
            n--;
        }
        return c;
    }
    
    public static void imprimir(Conjunto<Integer> c){
        for(Integer x: c){
            System.out.print(x + " ");
        }
    }
}
