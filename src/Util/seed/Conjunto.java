/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Iterator;

/**
 * Wrapper (Envoltorio)
 *
 * @author docente
 */
public class Conjunto<T> implements IConjunto<T>,  Iterable<T> {

    private ListaS<T> elementos = new ListaS();

    public Conjunto() {
    }

    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        ListaS<T> lista = new ListaS();
        Iterator<T> it = c1.elementos.iterator();
        Iterator<T> it2 = this.elementos.iterator();
        boolean noEncontrado = true;
        while (it.hasNext()) {
            T info = it.next();
            while (it2.hasNext()) {
                T info2 = it2.next();
                if (info.equals(info2)) {
                    noEncontrado = false;
                    break;
                }
            }
            if (noEncontrado) {
                lista.insertarInicio(info);
            }
                noEncontrado = true;
        }
        this.elementos.addAll(lista);
        return this;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Conjunto<Conjunto<T>> getPares(Conjunto<T> c1) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public T get(int i) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void set(int i, T info) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insertar(T info) {
        this.elementos.insertarInicio(info);
    }

    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String toString() {
        return "Conjunto{" + elementos + '}';
    }
     public Iterator<T> iterator() {
        return new IteratorConjunto(this.elementos.getPos(0));
    }
}
